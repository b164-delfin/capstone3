import './App.css';
import Navbar from './components/Navbar';
import Login from './components/Login';
import Register from './components/Register';
import {Route, Switch} from 'react-router';

function App() {
  return (
    <>
    <Navbar/>
    <Switch>
      <Router exact path="" component={Login} />
      <Router exact path="" component={Register} />
    </Switch>  
    </>
  );
}

export default App;
